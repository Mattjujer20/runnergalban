﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    /* Se obtiene la ref del rb */
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    /* Agrega fuerza en direccion negativa (hacia el jugador)
     * al llamar la funcion Outofbounds verifica si se fue de los limites*/
    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
    }

    /* Verifica si la posicion de los enemigos es menor a 15
     * si es menor a 15 los destruye*/
    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
