using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUpDown : MonoBehaviour
{
    bool tengoQueBajar = false;
    public int rapidez = 3;

    /* En cada frame verifica la posicion del enemigo en el eje Y, 
     * si el valor es igual a 5 se ejecuta la variable y si es igual a 0 no se ejecuta */
    private void Update()
    {
        if(transform.position.y > 5)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 0) 
        {
            tengoQueBajar = false;
        }
        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
       
    }

    /* Mueve el enemigo hacia arriba */
    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    /* Mueve el enemigo hacia abajo */
    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
