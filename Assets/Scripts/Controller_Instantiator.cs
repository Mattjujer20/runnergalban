﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;

    /* Se establece la velocidad de los enemigos */
    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
    }

    /* Llama a la funcion swapwnenemies para instanciar los enemigos
     * y la funcion de changevelocity para cambiarles la velocidad*/
    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
    }

    /* Aumena el TIME respecto al tiempo transcurrido, 
     * hace el cambio de velocidad con el math.smoothStep()*/
    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    /* Si el temporizador llega a 0 instancia un enemigo aleatorio de la lista  */
    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }
    }
}
