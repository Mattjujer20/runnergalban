﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;

    /* Obtiene la posicion en el eje X y 
     * la longitud para calcular cuando debe reinicarlo */
    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    /* Verifica si el game over es verdadero, si es asi se ejecuta junto con el canvas,
     * Se hace el efeco parallax debido a que se mueve dentro del eje X
     * si es menor que 20 se reinicia*/
    void Update()
    {
        if (Controller_Hud.gameOver == true) return;
        
        
        transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
        if (transform.localPosition.x < -20)
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
        }
    }
}
