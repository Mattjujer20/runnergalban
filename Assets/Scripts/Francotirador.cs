using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Francotirador : MonoBehaviour
{
    [SerializeField]
    public Transform _objetivo;
    [SerializeField]
    private GameObject bala;
    public float temporizador = 3f;
    public float contTemporizador = 0f;
    //private int numeroDeBalas;
    private int maximoDeDisparos = 15;

    /* Ayuda a detectar el transform del Player para los prefabs 
     * debido a que no estan en la escena */
    private void Awake()
    {
        _objetivo = Controller_Player.PlayerTransform;
    }

    /* Esta funcion instancia las balas para el objetivo */
    private void Start()
    {
        StartCoroutine(DisparoEnemigos());
    }

    /* Esta coroutine es un bucle que instancia la bala del francotirador */
    IEnumerator DisparoEnemigos()
    {
        //Debug.Log("Inicio coroutine");
        for (int i = 0; i < maximoDeDisparos; i++)
        {
            //numeroDeBalas++;           
            var balaInst = Instantiate(bala, transform.position, Quaternion.identity).GetComponent<Bala>();
            balaInst.objetivo = _objetivo.gameObject;
            yield return new WaitForSeconds(temporizador);
        }
        //Debug.Log("Fin coroutine");
    }

    /* En cada frame verifica si hay un objetivo, si la hay calcula la direccion del objetivo
     * y el francotirador va rotando (lo sigue con la mirada)*/
    void Update()
    {
        if (!_objetivo)
        {
            return;
        }
        Vector3 orientacionObjetivo = _objetivo.position - transform.position;
        Debug.DrawRay(transform.position, orientacionObjetivo, Color.red);

        transform.rotation = Quaternion.LookRotation(orientacionObjetivo);
    }
}
