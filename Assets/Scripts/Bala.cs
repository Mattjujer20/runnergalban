using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    public GameObject objetivo;
    private Vector3 direccion;
    public float rapidez;

    /* Calcula la direccion desde la posicion de la bala hacia el objetivo (Player)  */
    private void Start()
    {
        direccion = transform.position - objetivo.transform.position;
    }

    /* Mueve la bala hacia el objetivo multiplicada con una velocidad constante */
    private void Update()
    {
        transform.Translate(-direccion * rapidez * Time.deltaTime);
    }
}
