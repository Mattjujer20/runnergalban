﻿using System.Collections;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    public float rapidez;
    private int i = 0;
    private bool floored;
    public static Transform PlayerTransform;

    /* Use el awake acompañado de la variable public static para poder
     * detectar el transform del player desde los prefabs */
    private void Awake()
    {
        Controller_Player.PlayerTransform = transform;
    }
    /*  */
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();
    }

    /* Se llaman a las funciones */
    private void GetInput()
    {
        Jump();
        Duck();
        Gravitty();
    }

    /* Esta funcion sirve para que el Player salte */
    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }
    /* esta funcion sirve para que el Player se agache y al "mantenerla" acelere para abajo,
     * como si estuviera aumentando su masa*/
    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    /* Esta funcion sirve para que el Player pueda activar y desactivar la gravedad a su gusto */
    private void Gravitty()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            rb.useGravity = false;
            rb.velocity = Vector3.zero;
        }
        else
        {
            rb.useGravity = true;
        }
    }

    /* Esto sirve para que un objeto con el tag Enemy al colisionar con nuestro player
     * salte el HUD de que perdio la partida*/
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    /* Verifica si esta colisionando con el piso */
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }

    /* Verifica si colisiono con el componente powerspeed 
     * y asi empezar la coroutine */
    private void OnTriggerEnter(Collider other)
    {
        var powerUp = other.GetComponent<PowerUpSpeed>();
        if (powerUp)
        {
            StartCoroutine(PowerUpSpeedCoroutine(powerUp.multiplicadorTamano, powerUp.multiplicadorVelocidad, powerUp.duracion));
            powerUp.gameObject.SetActive(false);
        }
    }

    /* Sirve para instanciar el powerUp */
    IEnumerator PowerUpSpeedCoroutine(float tamano, float velocidad, float duracion)
    {
        Vector3 tamanoOriginal = transform.localScale;
        float rapidezOriginal = rapidez;
        transform.localScale = transform.localScale * tamano;
        rapidez = rapidez * velocidad;
        yield return new WaitForSeconds(duracion);
        transform.localScale = tamanoOriginal;
        rapidez = rapidezOriginal;

    }
}
