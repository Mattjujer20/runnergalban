﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    private float distance = 0;

    /* Se inicializan los textos, establece el texto de la distancia
     * y va cambiando a medida que avanza el juego y se desactiva 
     * el texto game over */
    void Start()
    {
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }

    /* Si gameOver es verdadero se detiene el juego actualizando la distancia recorrida
     * y si es falso va incrementando la distancia recorrida */
    void Update()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + Mathf.RoundToInt(distance).ToString();
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            distance += Time.deltaTime;
            distanceText.text = Mathf.RoundToInt(distance).ToString();
        }
    }
}
